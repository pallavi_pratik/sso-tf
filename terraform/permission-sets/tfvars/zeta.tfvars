aws_region = "ap-south-1"

permission_set  = {
  "admin_permission_set" : ["arn:aws:iam::aws:policy/AdministratorAccess"],
  "read_only_permission_set" : ["arn:aws:iam::aws:policy/ReadOnlyAccess"],
  "ops_infra_admin_permission_set": ["arn:aws:iam::aws:policy/AdministratorAccess"],
  "s3_fullaccess_permission_set": ["arn:aws:iam::aws:policy/AmazonS3FullAccess"]
}

inline_pm = {
  "network_admin" : "network-access",
}

ecr_s3 = {
  "ecr_s3_pm" : "ecr-s3-access"
}

zeus_rcv2_stage_bucket_access = {
  "zeus_rcv2_stage_bucket_access" : "zeus_rcv2_stage_bucket_access"
}


s3_encrypt_access = {
  "s3_encrypt_access" : "s3_encrypt_policy"
}

billing_read_aws_support_access = {
  "billing_read_aws_support_access" : "billing_read_aws_support_access_policy"
}

s3_read_download_access = {
  "s3_read_download_access" : "s3_read_download_access_policy"
}

dba_admin_permission_set = {
  "dba_admin_permission_set" : "dba_admin_permission_set_policy"
}

datascience_permission_set = {
  name                          = "datascience_permission_set"
  managed_permission_set        = [
    "arn:aws:iam::aws:policy/AmazonElasticMapReduceReadOnlyAccess",
    "arn:aws:iam::aws:policy/AmazonAthenaFullAccess",
    "arn:aws:iam::aws:policy/ReadOnlyAccess"
  ]
  inline_permission_set         = [
    "ds_s3_all_access"
  ]
}
dataengineering_permission_set = {
  name                          = "dataengineering_permission_set"
  managed_permission_set        = [
    "arn:aws:iam::aws:policy/AmazonEMRFullAccessPolicy_v2",
    "arn:aws:iam::aws:policy/AmazonAthenaFullAccess",
    "arn:aws:iam::aws:policy/AmazonRedshiftFullAccess",
    "arn:aws:iam::aws:policy/ReadOnlyAccess"
  ]
  inline_permission_set         = [
    "de_inline_pm"
  ]
}

athena_redshift_emr_access = {
  "athena_redshift_emr_access" : "athena_redshift_emr_access_policy"
}

dba_permission_set = {
  name                          = "dba_permission_set"
  managed_permission_set        = [
    "arn:aws:iam::aws:policy/ReadOnlyAccess",
    "arn:aws:iam::aws:policy/job-function/DatabaseAdministrator",
    "arn:aws:iam::aws:policy/AWSKeyManagementServicePowerUser",
    "arn:aws:iam::aws:policy/AmazonRDSFullAccess"
  ]
  inline_permission_set         = [
    "dba_inline_access"
  ]
}


data_devops_permission_set = {
  name = "data_devops_permission_set"
  managed_permission_set = [
    "arn:aws:iam::aws:policy/ReadOnlyAccess",
    "arn:aws:iam::aws:policy/AmazonMSKFullAccess",
    "arn:aws:iam::aws:policy/AmazonRedshiftFullAccess",
    "arn:aws:iam::aws:policy/CloudWatchFullAccess",
    "arn:aws:iam::aws:policy/AmazonRDSFullAccess",
    "arn:aws:iam::aws:policy/AmazonS3FullAccess",
    "arn:aws:iam::aws:policy/AmazonEMRFullAccessPolicy_v2",
    "arn:aws:iam::aws:policy/AmazonAthenaFullAccess",
    "arn:aws:iam::aws:policy/SecretsManagerReadWrite",
    "arn:aws:iam::aws:policy/AmazonEC2FullAccess",
  ]
  inline_permission_set = [
    "data_devops_inline_pm"
  ]
}

cloudfront_waf_permission_set = {
  "cloudfront_waf_permission_set" : "cloudfront_waf_access_policy"
}

admin_pm_sso_permission_set = {
  "admin_pm_sso_permission_set" : "admin_persmission_sso_access_policy"
}

admin_exp_permission_set = {
  "admin_exp_permission_set" : "admin_exp_persmission_access_policy"
}

admin_sso_permission_set = {
  "admin_sso_permission_set" : "admin_sso_persmission_access_policy"
}

admin_mgmt_permission_set = {
  "admin_mgmt_permission_set" : "admin_mgmt_persmission_access_policy"
}

datasync_iam_permission_set = {
  "datasync_iam_permission_set" : "datasync_iam_access_policy"
}

cf_route53_permission_set = {
  "cf_route53_permission_set" : "cf_route53_permission_access_policy"
}