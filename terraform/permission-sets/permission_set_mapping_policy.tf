locals{
    association-list = flatten([
      for each_name in keys(var.permission_set) :[
        for policy in var.permission_set[each_name]:{
          permission_set_name : each_name
          policy : policy
        }
    ]
  ])
}



locals {
    flatten-list  =  flatten([
      for each_account_id in keys(var.account_id) : [
        for each_user in keys(var.account_id[each_account_id]) : [
          for each_permission_set in var.account_id[each_account_id][each_user] : {
            permissions_set_name = each_permission_set
            account_id = each_account_id
            user_name = each_user
          }
        ]
      ]
    ])
}
