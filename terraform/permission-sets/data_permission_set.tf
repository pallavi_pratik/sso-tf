resource "aws_ssoadmin_permission_set" "zeus_rcv2_stage_bucket_access" {
  for_each         = var.zeus_rcv2_stage_bucket_access
  name             = each.key
  description      = "Permission Set Name ${each.key}"
  instance_arn     = tolist(data.aws_ssoadmin_instances.this.arns)[0]
  relay_state      = "https://console.aws.amazon.com/console/home?region=ap-south-1#"
  session_duration = "PT12H"
}

resource "aws_ssoadmin_permission_set_inline_policy" "zeus_rcv2_stage_bucket_access" {
  for_each           = var.zeus_rcv2_stage_bucket_access
  inline_policy      = data.aws_iam_policy_document.zeus_rcv2_stage_bucket_access.json
  instance_arn       = tolist(data.aws_ssoadmin_instances.this.arns)[0]
  permission_set_arn = aws_ssoadmin_permission_set.zeus_rcv2_stage_bucket_access[each.key].arn
}


data "aws_iam_policy_document" "zeus_rcv2_stage_bucket_access" {
  statement {
    sid = "1"

    actions = [
      "s3:ListAllMyBuckets",
      "s3:GetBucketLocation"
    ]

    resources = [
      "arn:aws:s3:::*",
    ]
  }

  statement {
    actions = [
      "s3:List*",
      "s3:Get*"
    ]

    resources = [
      "arn:aws:s3:::zeta-zeus-reportcenterv2-aws-default-staging-mumbai",
      "arn:aws:s3:::zeta-zeus-reportcenterv2-aws-default-staging-mumbai/*"
    ]
  }
}

resource "aws_ssoadmin_permission_set" "datascience_permission_set" {
  name             = var.datascience_permission_set.name
  description      = "Permission Set Name ${var.datascience_permission_set.name}"
  instance_arn     = tolist(data.aws_ssoadmin_instances.this.arns)[0]
  relay_state      = "https://console.aws.amazon.com/console/home?region=ap-south-1#"
  session_duration = "PT12H"
}

resource "aws_ssoadmin_permission_set_inline_policy" "datascience_permission_policy" {
  for_each           = toset(var.datascience_permission_set.inline_permission_set)
  inline_policy      = file("policy-json/${each.value}.json")
  instance_arn       = tolist(data.aws_ssoadmin_instances.this.arns)[0]
  permission_set_arn = aws_ssoadmin_permission_set.datascience_permission_set.arn
}

resource "aws_ssoadmin_managed_policy_attachment" "datascience_managed_permission_policy" {
  for_each           = toset(var.datascience_permission_set.managed_permission_set)
  managed_policy_arn = each.value
  instance_arn       = tolist(data.aws_ssoadmin_instances.this.arns)[0]
  permission_set_arn = aws_ssoadmin_permission_set.datascience_permission_set.arn
}

resource "aws_ssoadmin_permission_set" "dataengineering_permission_set" {
  name             = var.dataengineering_permission_set.name
  description      = "Permission Set Name ${var.dataengineering_permission_set.name}"
  instance_arn     = tolist(data.aws_ssoadmin_instances.this.arns)[0]
  relay_state      = "https://console.aws.amazon.com/console/home?region=ap-south-1#"
  session_duration = "PT12H"
}

resource "aws_ssoadmin_permission_set_inline_policy" "dataengineering_permission_set" {
  for_each           = toset(var.dataengineering_permission_set.inline_permission_set)
  inline_policy      = file("policy-json/${each.value}.json")
  instance_arn       = tolist(data.aws_ssoadmin_instances.this.arns)[0]
  permission_set_arn = aws_ssoadmin_permission_set.dataengineering_permission_set.arn
}

resource "aws_ssoadmin_managed_policy_attachment" "dataengineering_managed_permission_policy" {
  for_each           = toset(var.dataengineering_permission_set.managed_permission_set)
  managed_policy_arn = each.value
  instance_arn       = tolist(data.aws_ssoadmin_instances.this.arns)[0]
  permission_set_arn = aws_ssoadmin_permission_set.dataengineering_permission_set.arn
}

#dba_permission

resource "aws_ssoadmin_permission_set" "dba_permission_set" {
  name             = var.dba_permission_set.name
  description      = "Permission Set Name ${var.dba_permission_set.name}"
  instance_arn     = tolist(data.aws_ssoadmin_instances.this.arns)[0]
  relay_state      = "https://console.aws.amazon.com/console/home?region=ap-south-1#"
  session_duration = "PT12H"
}

resource "aws_ssoadmin_permission_set_inline_policy" "dba_permission_set" {
  for_each           = toset(var.dba_permission_set.inline_permission_set)
  inline_policy      = file("policy-json/${each.value}.json")
  instance_arn       = tolist(data.aws_ssoadmin_instances.this.arns)[0]
  permission_set_arn = aws_ssoadmin_permission_set.dba_permission_set.arn
}

resource "aws_ssoadmin_managed_policy_attachment" "dba_managed_permission_policy" {
  for_each           = toset(var.dba_permission_set.managed_permission_set)
  managed_policy_arn = each.value
  instance_arn       = tolist(data.aws_ssoadmin_instances.this.arns)[0]
  permission_set_arn = aws_ssoadmin_permission_set.dba_permission_set.arn
}

resource "aws_ssoadmin_permission_set" "data_devops_permission_set" {
  name             = var.data_devops_permission_set.name
  description      = "Permission Set Name ${var.data_devops_permission_set.name}"
  instance_arn     = tolist(data.aws_ssoadmin_instances.this.arns)[0]
  relay_state      = "https://console.aws.amazon.com/console/home?region=ap-south-1#"
  session_duration = "PT12H"
}

resource "aws_ssoadmin_permission_set_inline_policy" "data_devops_permission_set" {
  for_each           = toset(var.data_devops_permission_set.inline_permission_set)
  inline_policy      = file("policy-json/${each.value}.json")
  instance_arn       = tolist(data.aws_ssoadmin_instances.this.arns)[0]
  permission_set_arn = aws_ssoadmin_permission_set.data_devops_permission_set.arn
}

resource "aws_ssoadmin_managed_policy_attachment" "data_devops_permission_set" {
  for_each           = toset(var.data_devops_permission_set.managed_permission_set)
  managed_policy_arn = each.value
  instance_arn       = tolist(data.aws_ssoadmin_instances.this.arns)[0]
  permission_set_arn = aws_ssoadmin_permission_set.data_devops_permission_set.arn
}
