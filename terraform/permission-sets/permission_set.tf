data "aws_ssoadmin_instances" "this" {}

resource "aws_ssoadmin_permission_set" "this" {
  count            = length(local.association-list)
  name             = local.association-list[count.index].permission_set_name
  description      = "Permission Set Name ${local.association-list[count.index].permission_set_name}"
  instance_arn     = tolist(data.aws_ssoadmin_instances.this.arns)[0]
  relay_state      = "https://console.aws.amazon.com/console/home?region=ap-south-1#"
  session_duration = "PT12H"
}


# resource "aws_iam_policy" "this" {
#   count =  length(local.association-list)
#   name        = local.association-list[count.index].policy
#   description = "Allows the permissions for the AWS secret engine root user"
#   policy      = file("policy-json/${local.association-list[count.index].policy}.json")
# }


resource "aws_ssoadmin_managed_policy_attachment" "this" {
  count =  length(local.association-list)
  instance_arn       = tolist(data.aws_ssoadmin_instances.this.arns)[0]
  #managed_policy_arn = aws_iam_policy.this[count.index].arn
  managed_policy_arn = local.association-list[count.index].policy
  permission_set_arn = aws_ssoadmin_permission_set.this[count.index].arn
}

resource "aws_ssoadmin_permission_set" "inline" {
  for_each         = var.inline_pm
  name             = each.key
  description      = "Permission Set Name ${each.key}"
  instance_arn     = tolist(data.aws_ssoadmin_instances.this.arns)[0]
  relay_state      = "https://console.aws.amazon.com/console/home?region=ap-south-1#"
  session_duration = "PT12H"
}

resource "aws_ssoadmin_permission_set_inline_policy" "networkaccess" {
  for_each           = var.inline_pm
  inline_policy      = data.aws_iam_policy_document.networkaccess.json
  instance_arn       = tolist(data.aws_ssoadmin_instances.this.arns)[0]
  permission_set_arn = aws_ssoadmin_permission_set.inline[each.key].arn
}


data "aws_iam_policy_document" "networkaccess" {
  statement {
    sid = "1"
    effect = "Allow"
    actions = ["cloudtrail:*","ec2:*","ram:*","support:*"]
    resources = ["*"]
  }
}


resource "aws_ssoadmin_permission_set" "ecr-s3-access" {
  for_each         = var.ecr_s3
  name             = each.key
  description      = "Permission Set Name ${each.key}"
  instance_arn     = tolist(data.aws_ssoadmin_instances.this.arns)[0]
  relay_state      = "https://console.aws.amazon.com/console/home?region=ap-south-1#"
  session_duration = "PT12H"
}

resource "aws_ssoadmin_permission_set_inline_policy" "ecr-s3-access" {
  for_each           = var.ecr_s3
  inline_policy      = data.aws_iam_policy_document.ecr-s3-access.json
  instance_arn       = tolist(data.aws_ssoadmin_instances.this.arns)[0]
  permission_set_arn = aws_ssoadmin_permission_set.ecr-s3-access[each.key].arn
}


data "aws_iam_policy_document" "ecr-s3-access" {
  statement {
    sid = "1"
    effect = "Allow"
    actions = ["s3:ListStorageLensConfigurations",
                "s3:ListAccessPointsForObjectLambda",
                "s3:GetAccessPoint",
                "s3:PutAccountPublicAccessBlock",
                "s3:GetAccountPublicAccessBlock",
                "s3:ListAllMyBuckets",
                "s3:ListAccessPoints",
                "s3:ListJobs",
                "s3:PutStorageLensConfiguration",
                "ecr:*",
                "s3:CreateJob",
                "s3:*",
                "inspector2:*",
                "dynamodb:*"
              ]
      resources = [
        "*",
        "arn:aws:s3:::esg-terraform-state",
        "arn:aws:s3:::*/*",
        "arn:aws:inspector2:::*/*"
        ]


  }
}

resource "aws_ssoadmin_permission_set" "s3_encrypt_access" {
  for_each         = var.s3_encrypt_access
  name             = each.key
  description      = "Permission Set Name ${each.key}"
  instance_arn     = tolist(data.aws_ssoadmin_instances.this.arns)[0]
  relay_state      = "https://console.aws.amazon.com/console/home?region=ap-south-1#"
  session_duration = "PT12H"
}

resource "aws_ssoadmin_permission_set_inline_policy" "s3_encrypt_access" {
  for_each           = var.s3_encrypt_access
  inline_policy      = data.aws_iam_policy_document.s3_encrypt_policy.json
  instance_arn       = tolist(data.aws_ssoadmin_instances.this.arns)[0]
  permission_set_arn = aws_ssoadmin_permission_set.s3_encrypt_access[each.key].arn
}


data "aws_iam_policy_document" "s3_encrypt_policy" {
  statement {
    sid = "1"
    effect = "Allow"
    actions = [
                "ec2:GetManagedPrefixListEntries",
                "ec2:ExportClientVpnClientConfiguration",
                "ec2:GetCapacityReservationUsage",
                "ec2:GetHostReservationPurchasePreview",
                "ec2:GetTransitGatewayAttachmentPropagations",
                "ec2:GetGroupsForCapacityReservation",
                "ec2:GetTransitGatewayPrefixListReferences",
                "ec2:GetConsoleScreenshot",
                "ec2:GetReservedInstancesExchangeQuote",
                "ec2:GetAssociatedEnclaveCertificateIamRoles",
                "ec2:GetConsoleOutput",
                "ec2:ExportClientVpnClientCertificateRevocationList",
                "ec2:GetPasswordData",
                "ec2:GetLaunchTemplateData",
                "ec2:SearchTransitGatewayRoutes",
                "ec2:GetAssociatedIpv6PoolCidrs",
                "ec2:SearchLocalGatewayRoutes",
                "ec2:GetTransitGatewayRouteTablePropagations",
                "ec2:GetManagedPrefixListAssociations",
                "ec2:GetTransitGatewayMulticastDomainAssociations",
                "ec2:SearchTransitGatewayMulticastGroups",
                "ec2:GetTransitGatewayRouteTableAssociations",
                "ec2:DescribeInstances",
                "ec2:GetEbsEncryptionByDefault",
                "ec2:DescribeCoipPools",
                "ec2:DescribeSnapshots",
                "ec2:DescribeLocalGatewayVirtualInterfaces",
                "ec2:DescribeNetworkInsightsPaths",
                "ec2:DescribeHostReservationOfferings",
                "ec2:DescribeTrafficMirrorSessions",
                "ec2:DescribeExportImageTasks",
                "ec2:DescribeTrafficMirrorFilters",
                "ec2:DescribeVolumeStatus",
                "ec2:DescribeLocalGatewayRouteTableVpcAssociations",
                "ec2:DescribeScheduledInstanceAvailability",
                "ec2:DescribeVolumes",
                "ec2:GetEbsDefaultKmsKeyId",
                "ec2:DescribeFpgaImageAttribute",
                "ec2:DescribeExportTasks",
                "ec2:DescribeTransitGatewayMulticastDomains",
                "ec2:DescribeManagedPrefixLists",
                "ec2:DescribeKeyPairs",
                "ec2:DescribeReservedInstancesListings",
                "ec2:DescribeCapacityReservations",
                "ec2:DescribeClientVpnRoutes",
                "ec2:DescribeSpotFleetRequestHistory",
                "ec2:DescribeVpcClassicLinkDnsSupport",
                "ec2:DescribeSnapshotAttribute",
                "ec2:DescribeIdFormat",
                "ec2:DescribeFastSnapshotRestores",
                "ec2:DescribeVolumeAttribute",
                "ec2:DescribeImportSnapshotTasks",
                "ec2:DescribeLocalGatewayVirtualInterfaceGroups",
                "ec2:DescribeVpcEndpointServicePermissions",
                "ec2:DescribeTransitGatewayAttachments",
                "ec2:DescribeScheduledInstances",
                "ec2:DescribeImageAttribute",
                "ec2:DescribeIpv6Pools",
                "ec2:DescribeFleets",
                "ec2:DescribeReservedInstancesModifications",
                "ec2:DescribeSubnets",
                "ec2:DescribeMovingAddresses",
                "ec2:DescribeFleetHistory",
                "ec2:DescribePrincipalIdFormat",
                "ec2:DescribeFlowLogs",
                "ec2:DescribeRegions",
                "ec2:DescribeTransitGateways",
                "ec2:DescribeVpcEndpointServices",
                "ec2:DescribeSpotInstanceRequests",
                "ec2:DescribeAddressesAttribute",
                "ec2:DescribeVpcAttribute",
                "ec2:DescribeInstanceTypeOfferings",
                "ec2:DescribeTrafficMirrorTargets",
                "ec2:DescribeTransitGatewayRouteTables",
                "ec2:DescribeAvailabilityZones",
                "ec2:DescribeNetworkInterfaceAttribute",
                "ec2:DescribeLocalGatewayRouteTables",
                "ec2:DescribeVpcEndpointConnections",
                "ec2:DescribeInstanceStatus",
                "ec2:DescribeHostReservations",
                "ec2:DescribeBundleTasks",
                "ec2:DescribeIdentityIdFormat",
                "ec2:DescribeClassicLinkInstances",
                "ec2:DescribeTransitGatewayConnects",
                "ec2:DescribeVpcEndpointConnectionNotifications",
                "ec2:DescribeSecurityGroups",
                "ec2:DescribeFpgaImages",
                "ec2:DescribeVpcs",
                "ec2:DescribeStaleSecurityGroups",
                "ec2:DescribeAggregateIdFormat",
                "ec2:DescribeVolumesModifications",
                "ec2:DescribeClientVpnConnections",
                "ec2:DescribeTransitGatewayConnectPeers",
                "ec2:DescribeByoipCidrs",
                "ec2:DescribeNetworkInsightsAnalyses",
                "ec2:DescribePlacementGroups",
                "ec2:DescribeInternetGateways",
                "ec2:GetSerialConsoleAccessStatus",
                "ec2:DescribeSpotDatafeedSubscription",
                "ec2:DescribeAccountAttributes",
                "ec2:DescribeNetworkInterfacePermissions",
                "ec2:DescribeReservedInstances",
                "ec2:DescribeNetworkAcls",
                "ec2:DescribeRouteTables",
                "ec2:DescribeClientVpnEndpoints",
                "ec2:DescribeEgressOnlyInternetGateways",
                "ec2:DescribeLaunchTemplates",
                "ec2:DescribeVpnConnections",
                "ec2:DescribeVpcPeeringConnections",
                "ec2:DescribeReservedInstancesOfferings",
                "ec2:DescribeFleetInstances",
                "ec2:DescribeClientVpnTargetNetworks",
                "ec2:DescribeVpcEndpointServiceConfigurations",
                "ec2:DescribePrefixLists",
                "ec2:DescribeInstanceCreditSpecifications",
                "ec2:DescribeVpcClassicLink",
                "ec2:DescribeLocalGatewayRouteTableVirtualInterfaceGroupAssociations",
                "ec2:DescribeInstanceTypes",
                "ec2:DescribeVpcEndpoints",
                "ec2:DescribeElasticGpus",
                "ec2:DescribeVpnGateways",
                "ec2:DescribeTransitGatewayPeeringAttachments",
                "ec2:GetDefaultCreditSpecification",
                "ec2:DescribeAddresses",
                "ec2:DescribeInstanceAttribute",
                "ec2:DescribeDhcpOptions",
                "ec2:DescribeSpotPriceHistory",
                "ec2:DescribeNetworkInterfaces",
                "ec2:DescribeCarrierGateways",
                "ec2:DescribeIamInstanceProfileAssociations",
                "ec2:DescribeTags",
                "ec2:GetCoipPoolUsage",
                "ec2:DescribeLaunchTemplateVersions",
                "ec2:DescribeImportImageTasks",
                "ec2:DescribeNatGateways",
                "ec2:DescribeCustomerGateways",
                "ec2:DescribeInstanceEventNotificationAttributes",
                "ec2:DescribeLocalGateways",
                "ec2:DescribeSpotFleetRequests",
                "ec2:DescribeHosts",
                "ec2:DescribeImages",
                "ec2:DescribeSpotFleetInstances",
                "ec2:DescribeSecurityGroupReferences",
                "ec2:DescribePublicIpv4Pools",
                "ec2:DescribeClientVpnAuthorizationRules",
                "ec2:DescribeTransitGatewayVpcAttachments",
                "ec2:DescribeConversionTasks",
                "ssm:*",
                "s3:ListAccessPointsForObjectLambda",
                "s3:GetObjectVersionTagging",
                "s3:GetStorageLensConfigurationTagging",
                "s3:GetObjectAcl",
                "s3:GetBucketObjectLockConfiguration",
                "s3:GetIntelligentTieringConfiguration",
                "s3:GetObjectVersionAcl",
                "s3:PutObjectTagging",
                "s3:GetBucketPolicyStatus",
                "s3:GetObjectRetention",
                "s3:GetBucketWebsite",
                "s3:GetJobTagging",
                "s3:ListJobs",
                "s3:GetBucketNotification",
                "s3:GetReplicationConfiguration",
                "s3:ListMultipartUploadParts",
                "s3:GetObject",
                "s3:DescribeJob",
                "s3:GetAnalyticsConfiguration",
                "s3:GetObjectVersionForReplication",
                "s3:GetAccessPointForObjectLambda",
                "s3:GetStorageLensDashboard",
                "s3:GetLifecycleConfiguration",
                "s3:GetAccessPoint",
                "s3:GetInventoryConfiguration",
                "s3:GetBucketTagging",
                "s3:GetAccessPointPolicyForObjectLambda",
                "s3:GetBucketLogging",
                "s3:ListBucketVersions",
                "s3:ReplicateTags",
                "s3:ListBucket",
                "s3:GetAccelerateConfiguration",
                "s3:GetBucketPolicy",
                "s3:GetEncryptionConfiguration",
                "s3:GetObjectVersionTorrent",
                "s3:GetBucketRequestPayment",
                "s3:GetAccessPointPolicyStatus",
                "s3:GetObjectTagging",
                "s3:GetMetricsConfiguration",
                "s3:GetBucketOwnershipControls",
                "s3:GetBucketPublicAccessBlock",
                "s3:ListBucketMultipartUploads",
                "s3:GetAccessPointPolicyStatusForObjectLambda",
                "s3:ListAccessPoints",
                "s3:GetBucketVersioning",
                "s3:GetBucketAcl",
                "s3:GetAccessPointConfigurationForObjectLambda",
                "s3:ListStorageLensConfigurations",
                "s3:GetObjectTorrent",
                "s3:GetStorageLensConfiguration",
                "s3:GetAccountPublicAccessBlock",
                "s3:ListAllMyBuckets",
                "s3:GetBucketCORS",
                "s3:GetBucketLocation",
                "s3:GetAccessPointPolicy",
                "s3:GetObjectVersion",

    ]
      resources = ["arn:aws:ec2:ap-south-1:286817589435:instance/i-050faf4029afa970b","*"]


  }
}


resource "aws_ssoadmin_permission_set" "dba_admin_permission_set" {
  for_each         = var.dba_admin_permission_set
  name             = each.key
  description      = "Permission Set Name ${each.key}"
  instance_arn     = tolist(data.aws_ssoadmin_instances.this.arns)[0]
  relay_state      = "https://console.aws.amazon.com/console/home?region=ap-south-1#"
  session_duration = "PT12H"
}

resource "aws_ssoadmin_permission_set_inline_policy" "dba_admin_permission_set" {
  for_each           = var.dba_admin_permission_set
  inline_policy      = data.aws_iam_policy_document.dba_admin_permission_set_policy.json
  instance_arn       = tolist(data.aws_ssoadmin_instances.this.arns)[0]
  permission_set_arn = aws_ssoadmin_permission_set.dba_admin_permission_set[each.key].arn
}


data "aws_iam_policy_document" "dba_admin_permission_set_policy" {
  statement {
    sid = "1"
    effect = "Allow"
    actions = ["rds:*"]
      resources = ["*"]
  }
}






# resource "aws_iam_policy" "this" {
#   count =  length(local.association-list)
#   name        = local.association-list[count.index].policy
#   description = "Allows the permissions for the AWS secret engine root user"
#   policy      = file("policy-json/${local.association-list[count.index].policy}.json")
# }

resource "aws_ssoadmin_permission_set" "athena_redshift_emr_access" {
  for_each         = var.athena_redshift_emr_access
  name             = each.key
  description      = "Permission Set Name ${each.key}"
  instance_arn     = tolist(data.aws_ssoadmin_instances.this.arns)[0]
  relay_state      = "https://console.aws.amazon.com/console/home?region=ap-south-1#"
  session_duration = "PT12H"
}


resource "aws_ssoadmin_permission_set_inline_policy" "athena_redshift_emr_access" {
  for_each           = var.athena_redshift_emr_access
  inline_policy      = data.aws_iam_policy_document.athena_redshift_emr_access_policy.json
  instance_arn       = tolist(data.aws_ssoadmin_instances.this.arns)[0]
  permission_set_arn = aws_ssoadmin_permission_set.athena_redshift_emr_access[each.key].arn
}

data "aws_iam_policy_document" "athena_redshift_emr_access_policy" {
  statement {
    sid = "1"
    effect = "Allow"
    actions = ["redshift:ListDatabases",
                "elasticmapreduce:DescribeNotebookExecution",
                "redshift:CreateClusterSecurityGroup",
                "athena:GetQueryResults",
                "athena:ListWorkGroups",
                "redshift:DescribeClusterSubnetGroups",
                "redshift:DescribeQuery",
                "elasticmapreduce:ListSecurityConfigurations",
                "redshift:BatchModifyClusterSnapshots",
                "redshift:DescribeClusterVersions",
                "redshift:DescribeStorage",
                "elasticmapreduce:DescribeSecurityConfiguration",
                "athena:CreateDataCatalog",
                "redshift:DescribeClusterSecurityGroups",
                "elasticmapreduce:DescribeRepository",
                "redshift:ViewQueriesInConsole",
                "athena:CreateWorkGroup",
                "elasticmapreduce:ModifyInstanceFleet",
                "redshift:RevokeSnapshotAccess",
                "redshift:CreateScheduledAction",
                "redshift:CancelQuery",
                "redshift:CreateEventSubscription",
                "elasticmapreduce:StartEditor",
                "elasticmapreduce:RemoveAutoScalingPolicy",
                "redshift:ModifyClusterParameterGroup",
                "redshift:AuthorizeClusterSecurityGroupIngress",
                "athena:BatchGetNamedQuery",
                "redshift:GetReservedNodeExchangeOfferings",
                "elasticmapreduce:ListStudios",
                "redshift:DeleteCluster",
                "redshift:PurchaseReservedNodeOffering",
                "elasticmapreduce:DescribeStudio",
                "redshift:ModifySnapshotCopyRetentionPeriod",
                "athena:GetWorkGroup",
                "elasticmapreduce:DescribeStep",
                "redshift:DescribeReservedNodeOfferings",
                "redshift:DescribeSavedQueries",
                "elasticmapreduce:DescribeEditor",
                "redshift:DescribeLoggingStatus",
                "redshift:AssociateDataShareConsumer",
                "elasticmapreduce:UpdateStudio",
                "redshift:ModifyClusterSubnetGroup",
                "redshift:ModifyScheduledAction",
                "elasticmapreduce:OpenEditorInConsole",
                "elasticmapreduce:ListInstanceFleets",
                "redshift:AuthorizeSnapshotAccess",
                "redshift:ListTables",
                "redshift:BatchDeleteClusterSnapshots",
                "redshift:DescribeReservedNodes",
                "redshift:CreateClusterParameterGroup",
                "elasticmapreduce:RemoveTags",
                "athena:GetQueryExecution",
                "redshift:EnableSnapshotCopy",
                "athena:ListTableMetadata",
                "elasticmapreduce:ListEditors",
                "redshift:CreateSnapshotCopyGrant",
                "redshift:ModifyUsageLimit",
                "redshift:DescribeHsmConfigurations",
                "redshift:RejectDataShare",
                "athena:GetNamedQuery",
                "redshift:DescribeClusterSnapshots",
                "elasticmapreduce:CreateStudio",
                "athena:StopQueryExecution",
                "elasticmapreduce:PutManagedScalingPolicy",
                "elasticmapreduce:ViewEventsFromAllClustersInConsole",
                "redshift:CreateHsmClientCertificate",
                "elasticmapreduce:GetStudioSessionMapping",
                "athena:GetPreparedStatement",
                "athena:ListNamedQueries",
                "redshift:ModifyClusterDbRevision",
                "elasticmapreduce:CreateRepository",
                "redshift:DescribeDataSharesForConsumer",
                "elasticmapreduce:DeleteStudioSessionMapping",
                "redshift:AuthorizeDataShare",
                "redshift:ListSavedQueries",
                "redshift:CancelQuerySession",
                "redshift:ModifyClusterSnapshot",
                "elasticmapreduce:UnlinkRepository",
                "redshift:ModifyCluster",
                "athena:ListPreparedStatements",
                "redshift:CreateUsageLimit",
                "elasticmapreduce:GetBlockPublicAccessConfiguration",
                "athena:ListDataCatalogs",
                "elasticmapreduce:DescribeCluster",
                "redshift:ModifyClusterMaintenance",
                "redshift:DescribeSnapshotSchedules",
                "redshift:DescribeDataSharesForProducer",
                "elasticmapreduce:TerminateJobFlows",
                "redshift:DescribeClusterParameters",
                "redshift:ModifyEventSubscription",
                "redshift:DeleteTags",
                "athena:TagResource",
                "redshift:RotateEncryptionKey",
                "athena:ListEngineVersions",
                "redshift:DisableLogging",
                "redshift:DescribeAccountAttributes",
                "redshift:DescribeHsmClientCertificates",
                "redshift:DescribeTags",
                "elasticmapreduce:PutAutoScalingPolicy",
                "athena:ListTagsForResource",
                "redshift:ListSchemas",
                "elasticmapreduce:StartNotebookExecution",
                "redshift:DescribeNodeConfigurationOptions",
                "redshift:CreateCluster",
                "redshift:DescribeDataShares",
                "elasticmapreduce:RunJobFlow",
                "elasticmapreduce:AddJobFlowSteps",
                "elasticmapreduce:ListStudioSessionMappings",
                "elasticmapreduce:ListBootstrapActions",
                "redshift:DescribeEvents",
                "redshift:ModifyClusterIamRoles",
                "elasticmapreduce:CreateEditor",
                "elasticmapreduce:SetTerminationProtection",
                "elasticmapreduce:StopNotebookExecution",
                "elasticmapreduce:ListRepositories",
                "elasticmapreduce:ListInstanceGroups",
                "elasticmapreduce:ListInstances",
                "elasticmapreduce:PutBlockPublicAccessConfiguration",
                "redshift:DescribeEventSubscriptions",
                "redshift:DescribeOrderableClusterOptions",
                "elasticmapreduce:ListSteps",
                "redshift:DescribeScheduledActions",
                "redshift:ResumeCluster",
                "redshift:RevokeClusterSecurityGroupIngress",
                "redshift:EnableLogging",
                "redshift:DescribeSnapshotCopyGrants",
                "redshift:GetClusterCredentials",
                "redshift:CreateHsmConfiguration",
                "elasticmapreduce:CancelSteps",
                "athena:BatchGetQueryExecution",
                "athena:UpdateDataCatalog",
                "athena:GetTableMetadata",
                "redshift:ModifySavedQuery",
                "redshift:CreateSnapshotSchedule",
                "redshift:DescribeEventCategories",
                "redshift:CreateClusterSnapshot",
                "redshift:DescribeTableRestoreStatus",
                "athena:CreatePreparedStatement",
                "athena:UntagResource",
                "redshift:CancelResize",
                "elasticmapreduce:ModifyCluster",
                "redshift:FetchResults",
                "elasticmapreduce:UpdateRepository",
                "redshift:DeauthorizeDataShare",
                "redshift:CreateTags",
                "athena:StartQueryExecution",
                "redshift:DescribeClusterTracks",
                "redshift:CreateSavedQuery",
                "redshift:DescribeUsageLimits",
                "elasticmapreduce:CreateSecurityConfiguration",
                "redshift:ModifySnapshotSchedule",
                "athena:UpdateWorkGroup",
                "elasticmapreduce:LinkRepository",
                "redshift:DescribeDefaultClusterParameters",
                "redshift:PauseCluster",
                "redshift:RestoreTableFromClusterSnapshot",
                "elasticmapreduce:RemoveManagedScalingPolicy",
                "athena:GetQueryResultsStream",
                "athena:UpdatePreparedStatement",
                "redshift:AcceptReservedNodeExchange",
                "redshift:DescribeClusterDbRevisions",
                "elasticmapreduce:ListNotebookExecutions",
                "elasticmapreduce:CreateStudioSessionMapping",
                "redshift:ViewQueriesFromConsole",
                "redshift:ResizeCluster",
                "athena:CreateNamedQuery",
                "athena:ListDatabases",
                "redshift:CopyClusterSnapshot",
                "redshift:DescribeResize",
                "redshift:JoinGroup",
                "elasticmapreduce:AddInstanceFleet",
                "elasticmapreduce:GetManagedScalingPolicy",
                "elasticmapreduce:UpdateStudioSessionMapping",
                "athena:GetDatabase",
                "athena:GetDataCatalog",
                "redshift:ResetClusterParameterGroup",
                "athena:ListQueryExecutions",
                "redshift:ModifyClusterSnapshotSchedule",
                "redshift:DisableSnapshotCopy",
                "elasticmapreduce:StopEditor",
                "redshift:ModifyAquaConfiguration",
                "elasticmapreduce:ListClusters",
                "redshift:CreateClusterUser",
                "redshift:DescribeClusterParameterGroups",
                "elasticmapreduce:AddInstanceGroups",
                "redshift:RebootCluster",
                "redshift:RestoreFromClusterSnapshot",
                "elasticmapreduce:ModifyInstanceGroups",
                "redshift:DescribeClusters",
                "redshift:ExecuteQuery",
                "redshift:CreateClusterSubnetGroup",
                "elasticmapreduce:AddTags",
                "redshift:DescribeTable",
                "elasticmapreduce:DescribeJobFlows",
                "redshift:DisassociateDataShareConsumer"]
      resources = ["*"]


  }
}

resource "aws_ssoadmin_permission_set" "cloudfront_waf_permission_set" {
  for_each         = var.cloudfront_waf_permission_set
  name             = each.key
  description      = "Permission Set Name ${each.key}"
  instance_arn     = tolist(data.aws_ssoadmin_instances.this.arns)[0]
  relay_state      = "https://console.aws.amazon.com/console/home?region=ap-south-1#"
}

resource "aws_ssoadmin_permission_set_inline_policy" "cloudfront_waf_permission_set" {
  for_each           = var.cloudfront_waf_permission_set
  inline_policy      = data.aws_iam_policy_document.cloudfront_waf_access_policy.json
  instance_arn       = tolist(data.aws_ssoadmin_instances.this.arns)[0]
  permission_set_arn = aws_ssoadmin_permission_set.cloudfront_waf_permission_set[each.key].arn
}

data "aws_iam_policy_document" "cloudfront_waf_access_policy" {
statement {
    sid = "1"
    effect = "Allow"
    actions = ["wafv2:*",
            "waf:*",
            "fms:*",
            "cloudfront:*",
            "s3:ListAllMyBuckets",
            "s3:PutBucketPolicy",
            "s3:Get*",
            "s3:List*",
            "acm:AddTagsToCertificate",
            "acm:DescribeCertificate",
            "acm:ExportCertificate",
            "acm:GetAccountConfiguration",
            "acm:GetCertificate",
            "acm:ImportCertificate",
            "acm:ListCertificates",
            "acm:ListTagsForCertificate",
            "acm:PutAccountConfiguration",
            "acm:RemoveTagsFromCertificate",
            "acm:RenewCertificate",
            "acm:RequestCertificate",
            "acm:ResendValidationEmail",
            "acm:UpdateCertificateOptions"]
    resources = ["*"]
  }
}

resource "aws_ssoadmin_permission_set" "admin_pm_sso_permission_set" {
  for_each         = var.admin_pm_sso_permission_set
  name             = each.key
  description      = "Permission Set Name ${each.key}"
  instance_arn     = tolist(data.aws_ssoadmin_instances.this.arns)[0]
  relay_state      = "https://console.aws.amazon.com/console/home?region=ap-south-1#"
}

resource "aws_ssoadmin_permission_set_inline_policy" "admin_pm_sso_permission_set" {
  for_each           = var.admin_pm_sso_permission_set
  inline_policy      = data.aws_iam_policy_document.admin_persmission_sso.json
  instance_arn       = tolist(data.aws_ssoadmin_instances.this.arns)[0]
  permission_set_arn = aws_ssoadmin_permission_set.admin_pm_sso_permission_set[each.key].arn
}


data "aws_iam_policy_document" "admin_persmission_sso" {
  statement {
      effect = "Allow"
      actions = ["*"]
      resources = ["*"]
    }
  statement  {
      sid = "Stmt1OrgPermissions"
      effect = "Deny"
      actions = ["organizations:*"]
      resources = ["*"]
   }
}

resource "aws_ssoadmin_permission_set" "admin_exp_permission_set" {
  for_each         = var.admin_exp_permission_set
  name             = each.key
  description      = "Permission Set Name ${each.key}"
  instance_arn     = tolist(data.aws_ssoadmin_instances.this.arns)[0]
  session_duration = "PT12H"
  relay_state      = "https://console.aws.amazon.com/console/home?region=ap-south-1#"
}

resource "aws_ssoadmin_permission_set_inline_policy" "admin_exp_permission_set" {
  for_each           = var.admin_exp_permission_set
  inline_policy      = data.aws_iam_policy_document.admin_exp_persmission_access_policy.json
  instance_arn       = tolist(data.aws_ssoadmin_instances.this.arns)[0]
  permission_set_arn = aws_ssoadmin_permission_set.admin_exp_permission_set[each.key].arn
}


data "aws_iam_policy_document" "admin_exp_persmission_access_policy" {
  statement {
      effect = "Allow"
      actions = ["*"]
      resources = ["*"]
    }
  statement  {
      effect = "Deny"
      actions = ["organizations:*"]
      resources = ["*"]
   }
}

resource "aws_ssoadmin_permission_set" "admin_sso_permission_set" {
  for_each         = var.admin_sso_permission_set
  name             = each.key
  description      = "Permission Set Name ${each.key}"
  instance_arn     = tolist(data.aws_ssoadmin_instances.this.arns)[0]
  session_duration = "PT12H"
  relay_state      = "https://console.aws.amazon.com/console/home?region=ap-south-1#"
}

resource "aws_ssoadmin_permission_set_inline_policy" "admin_sso_permission_set" {
  for_each           = var.admin_sso_permission_set
  inline_policy      = data.aws_iam_policy_document.admin_sso_permission_access_policy.json
  instance_arn       = tolist(data.aws_ssoadmin_instances.this.arns)[0]
  permission_set_arn = aws_ssoadmin_permission_set.admin_sso_permission_set[each.key].arn
}


data "aws_iam_policy_document" "admin_sso_permission_access_policy" {
  statement {
      effect = "Allow"
      actions = ["*"]
      resources = ["*"]
    }
  statement  {
      sid = "Stmt1OrgPermissions"
      effect = "Deny"
      actions = ["organizations:*"]
      resources = ["*"]
   }
}

resource "aws_ssoadmin_permission_set" "admin_mgmt_permission_set" {
  for_each         = var.admin_mgmt_permission_set
  name             = each.key
  description      = "Permission Set Name ${each.key}"
  session_duration = "PT12H"
  instance_arn     = tolist(data.aws_ssoadmin_instances.this.arns)[0]
  relay_state      = "https://console.aws.amazon.com/console/home?region=ap-south-1#"
}

resource "aws_ssoadmin_permission_set_inline_policy" "admin_mgmt_permission_set" {
  for_each           = var.admin_mgmt_permission_set
  inline_policy      = data.aws_iam_policy_document.admin_mgmt_permission_access_policy.json
  instance_arn       = tolist(data.aws_ssoadmin_instances.this.arns)[0]
  permission_set_arn = aws_ssoadmin_permission_set.admin_mgmt_permission_set[each.key].arn
}


data "aws_iam_policy_document" "admin_mgmt_permission_access_policy" {
  statement {
      effect = "Allow"
      actions = ["*"]
      resources = ["*"]
    }
}

resource "aws_ssoadmin_permission_set" "datasync_iam_permission_set" {
  for_each         = var.datasync_iam_permission_set
  name             = each.key
  description      = "Permission Set Name ${each.key}"
  instance_arn     = tolist(data.aws_ssoadmin_instances.this.arns)[0]
  relay_state      = "https://console.aws.amazon.com/console/home?region=ap-south-1#"
}

resource "aws_ssoadmin_permission_set_inline_policy" "datasync_iam_permission_set" {
  for_each           = var.datasync_iam_permission_set
  inline_policy      = data.aws_iam_policy_document.datasync_iam_access_policy.json
  instance_arn       = tolist(data.aws_ssoadmin_instances.this.arns)[0]
  permission_set_arn = aws_ssoadmin_permission_set.datasync_iam_permission_set[each.key].arn
}

data "aws_iam_policy_document" "datasync_iam_access_policy" {
statement {
    sid = "1"
    effect = "Allow"
    actions = [
      "iam:AddRoleToInstanceProfile",
      "iam:AttachRolePolicy",
      "iam:AttachUserPolicy",
      "iam:CreateUser",
      "iam:CreateAccessKey",
      "iam:CreateInstanceProfile",
      "iam:CreateLoginProfile",
      "iam:CreatePolicy",
      "iam:CreatePolicyVersion",
      "iam:CreateRole",
      "iam:CreateSAMLProvider",
      "iam:CreateServiceLinkedRole",
      "iam:CreateServiceSpecificCredential",
      "iam:GetAccessKeyLastUsed",
      "iam:GetAccountAuthorizationDetails",
      "iam:GetAccountPasswordPolicy",
      "iam:GetAccountSummary",
      "iam:GetContextKeysForCustomPolicy",
      "iam:GetContextKeysForPrincipalPolicy",
      "iam:GetCredentialReport",
      "iam:GetGroup",
      "iam:GetGroupPolicy",
      "iam:GetInstanceProfile",
      "iam:GetLoginProfile",
      "iam:GetOpenIDConnectProvider",
      "iam:GetPolicy",
      "iam:GetPolicyVersion",
      "iam:GetRole",
      "iam:GetRolePolicy",
      "iam:GetSAMLProvider",
      "iam:GetSSHPublicKey",
      "iam:GetServerCertificate",
      "iam:GetServiceLastAccessedDetails",
      "iam:GetServiceLastAccessedDetailsWithEntities",
      "iam:GetServiceLinkedRoleDeletionStatus",
      "iam:GetUser",
      "iam:GetUserPolicy",
      "iam:ListAccessKeys",
      "iam:ListAccountAliases",
      "iam:ListAttachedGroupPolicies",
      "iam:ListAttachedRolePolicies",
      "iam:ListAttachedUserPolicies",
      "iam:ListEntitiesForPolicy",
      "iam:ListGroupPolicies",
      "iam:ListGroups",
      "iam:ListGroupsForUser",
      "iam:ListInstanceProfiles",
      "iam:ListInstanceProfilesForRole",
      "iam:ListMFADevices",
      "iam:ListOpenIDConnectProviders",
      "iam:ListPolicies",
      "iam:ListPoliciesGrantingServiceAccess",
      "iam:ListPolicyVersions",
      "iam:ListRolePolicies",
      "iam:ListRoles",
      "iam:ListSAMLProviders",
      "iam:ListSSHPublicKeys",
      "iam:ListServerCertificates",
      "iam:ListServiceSpecificCredentials",
      "iam:ListSigningCertificates",
      "iam:ListUserPolicies",
      "iam:ListUsers",
      "iam:DeletePolicyVersion",
      "iam:ListVirtualMFADevices",
      "iam:PassRole",
      "iam:PutGroupPolicy",
      "iam:PutRolePolicy",
      "iam:PutUserPolicy",
      "iam:SetDefaultPolicyVersion",
      "iam:SimulateCustomPolicy",
      "iam:SimulatePrincipalPolicy",
      "iam:UpdateAccessKey",
      "iam:UpdateAccountPasswordPolicy",
      "iam:UpdateAssumeRolePolicy",
      "iam:UpdateRoleDescription",
      "datasync:CancelTaskExecution",
      "datasync:CreateAgent",
      "datasync:CreateLocationS3",
      "datasync:CreateTask",
      "datasync:DeleteLocation",
      "datasync:DeleteTask",
      "datasync:DescribeAgent",
      "datasync:DescribeLocationObjectStorage",
      "datasync:DescribeLocationS3",
      "datasync:DescribeTask",
      "datasync:DescribeTaskExecution",
      "datasync:ListAgents",
      "datasync:ListLocations",
      "datasync:ListTagsForResource",
      "datasync:ListTaskExecutions",
      "datasync:ListTasks",
      "datasync:StartTaskExecution",
      "datasync:TagResource",
      "datasync:UntagResource",
      "datasync:UpdateLocationObjectStorage",
      "datasync:UpdateTask",
      "datasync:UpdateTaskExecution",
      "sts:AssumeRole",
      "sts:GetCallerIdentity",
      "s3:Get*",
      "s3:List*",
      "s3:AbortMultipartUpload",
      "s3:DeleteObject",
      "s3:PutObjectTagging",
      "s3:PutObject",
      "s3:AbortMultipartUpload",
      "s3:ObjectOwnerOverrideToBucketOwner",
      "s3:PutAccelerateConfiguration",
      "s3:PutAnalyticsConfiguration",
      "s3:PutBucketAcl",
      "s3:PutBucketCORS",
      "s3:PutBucketLogging",
      "s3:PutBucketNotification",
      "s3:PutBucketPolicy",
      "s3:PutBucketRequestPayment",
      "s3:PutBucketTagging",
      "s3:PutBucketVersioning",
      "s3:PutBucketWebsite",
      "s3:PutInventoryConfiguration",
      "s3:PutLifecycleConfiguration",
      "s3:PutMetricsConfiguration",
      "s3:PutObject",
      "s3:PutObjectAcl",
      "s3:PutObjectTagging",
      "s3:PutObjectVersionAcl",
      "s3:PutObjectVersionTagging",
      "s3:PutReplicationConfiguration",
      "s3:ReplicateDelete",
      "s3:ReplicateObject",
      "s3:ReplicateTags",
      "s3:RestoreObject"
]
    resources = ["arn:aws:s3:::*/*",
    "arn:aws:iam::813361731051:role/*",
    "arn:aws:iam::813361731051:policy/*",
    "arn:aws:sts::813361731051:assumed-role/*",
    "arn:aws:datasync:ap-south-1:813361731051:location/*",
    "arn:aws:datasync:ap-south-1:813361731051:task/*",
    "arn:aws:sts::813361731051:*"
    ]
  }
}

resource "aws_ssoadmin_permission_set" "cf_route53_permission_set" {
  for_each         = var.cf_route53_permission_set
  name             = each.key
  description      = "Permission Set Name ${each.key}"
  session_duration = "PT12H"
  instance_arn     = tolist(data.aws_ssoadmin_instances.this.arns)[0]
  relay_state      = "https://console.aws.amazon.com/console/home?region=ap-south-1#"
}

resource "aws_ssoadmin_permission_set_inline_policy" "cf_route53_permission_set" {
  for_each           = var.cf_route53_permission_set
  inline_policy      = data.aws_iam_policy_document.cf_route53_permission_access_policy.json
  instance_arn       = tolist(data.aws_ssoadmin_instances.this.arns)[0]
  permission_set_arn = aws_ssoadmin_permission_set.cf_route53_permission_set[each.key].arn
}


data "aws_iam_policy_document" "cf_route53_permission_access_policy" {
    statement {
            effect = "Allow"
            actions = [
                "route53resolver:ListResolverRules",
                "route53resolver:ListResolverRuleAssociations",
                "route53resolver:ListResolverEndpoints",
                "route53resolver:ListFirewallDomainLists",
                "cloudfront:*",
                "route53domains:*",
                "s3:*",
                "route53-recovery-readiness:*",
                "waf:*",
                "route53-recovery-control-config:*",
                "wafv2:*",
                "route53:*",
                "waf-regional:*",
                "route53-recovery-cluster:*"
            ]
            resources = ["*"]
        }
}