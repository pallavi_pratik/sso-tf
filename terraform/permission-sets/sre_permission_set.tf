
resource "aws_ssoadmin_permission_set" "billing_read_aws_support_access" {
  for_each         = var.billing_read_aws_support_access
  name             = each.key
  description      = "Permission Set Name ${each.key}"
  instance_arn     = tolist(data.aws_ssoadmin_instances.this.arns)[0]
  relay_state      = "https://console.aws.amazon.com/console/home?region=ap-south-1#"
  session_duration = "PT12H"
}

resource "aws_ssoadmin_permission_set_inline_policy" "billing_read_aws_support_access" {
  for_each           = var.billing_read_aws_support_access
  inline_policy      = data.aws_iam_policy_document.billing_read_aws_support_access_policy.json
  instance_arn       = tolist(data.aws_ssoadmin_instances.this.arns)[0]
  permission_set_arn = aws_ssoadmin_permission_set.billing_read_aws_support_access[each.key].arn
}


data "aws_iam_policy_document" "billing_read_aws_support_access_policy" {
  statement {
    sid = "1"
    effect = "Allow"
    actions = [
      "support:AddAttachmentsToSet",
      "support:AddCommunicationToCase",
      "support:DescribeTrustedAdvisorCheckRefreshStatuses",
      "support:CreateCase",
      "support:DescribeTrustedAdvisorCheckResult",
      "support:DescribeTrustedAdvisorChecks",
      "aws-portal:ViewUsage",
      "support:RateCaseCommunication",
      "aws-portal:ViewPaymentMethods",
      "support:DescribeSeverityLevels",
      "support:DescribeSupportLevel",
      "support:InitiateCallForCase",
      "aws-portal:ViewBilling",
      "support:DescribeCaseAttributes",
      "support:RefreshTrustedAdvisorCheck",
      "support:DescribeTrustedAdvisorCheckSummaries",
      "support:SearchForCases",
      "support:DescribeCases",
      "aws-portal:ViewAccount",
      "support:DescribeServices",
      "support:PutCaseAttributes",
      "support:DescribeAttachment",
      "support:DescribeCommunications",
      "support:InitiateChatForCase",
      "support:DescribeIssueTypes"
    ]
      resources = ["*"]
  }
}




resource "aws_ssoadmin_permission_set" "s3_read_download_access" {
  for_each         = var.s3_read_download_access
  name             = each.key
  description      = "Permission Set Name ${each.key}"
  instance_arn     = tolist(data.aws_ssoadmin_instances.this.arns)[0]
  relay_state      = "https://console.aws.amazon.com/console/home?region=ap-south-1#"
  session_duration = "PT12H"
}

resource "aws_ssoadmin_permission_set_inline_policy" "s3_read_download_access" {
  for_each           = var.s3_read_download_access
  inline_policy      = data.aws_iam_policy_document.s3_read_download_access_policy.json
  instance_arn       = tolist(data.aws_ssoadmin_instances.this.arns)[0]
  permission_set_arn = aws_ssoadmin_permission_set.s3_read_download_access[each.key].arn
}


data "aws_iam_policy_document" "s3_read_download_access_policy" {
  statement {
    sid = "1"
    effect = "Allow"
    actions = [
      "s3:ListAccessPointsForObjectLambda",
                "s3:PutAnalyticsConfiguration",
                "s3:PutAccessPointConfigurationForObjectLambda",
                "s3:GetObjectVersionTagging",
                "s3:PutStorageLensConfiguration",
                "s3:CreateBucket",
                "s3:GetStorageLensConfigurationTagging",
                "s3:ReplicateObject",
                "s3:GetObjectAcl",
                "s3:GetBucketObjectLockConfiguration",
                "s3:GetIntelligentTieringConfiguration",
                "s3:PutLifecycleConfiguration",
                "s3:GetObjectVersionAcl",
                "s3:PutObjectTagging",
                "s3:GetBucketPolicyStatus",
                "s3:GetObjectRetention",
                "s3:GetBucketWebsite",
                "s3:GetJobTagging",
                "s3:ListJobs",
                "s3:PutReplicationConfiguration",
                "s3:PutObjectLegalHold",
                "s3:GetObjectLegalHold",
                "s3:GetBucketNotification",
                "s3:PutBucketCORS",
                "s3:GetReplicationConfiguration",
                "s3:ListMultipartUploadParts",
                "s3:PutObject",
                "s3:GetObject",
                "s3:PutBucketNotification",
                "s3:DescribeJob",
                "s3:PutBucketLogging",
                "s3:GetAnalyticsConfiguration",
                "s3:PutBucketObjectLockConfiguration",
                "s3:GetObjectVersionForReplication",
                "s3:CreateJob",
                "s3:GetAccessPointForObjectLambda",
                "s3:GetStorageLensDashboard",
                "s3:CreateAccessPoint",
                "s3:GetLifecycleConfiguration",
                "s3:GetAccessPoint",
                "s3:GetInventoryConfiguration",
                "s3:GetBucketTagging",
                "s3:PutAccelerateConfiguration",
                "s3:GetAccessPointPolicyForObjectLambda",
                "s3:GetBucketLogging",
                "s3:ListBucketVersions",
                "s3:ReplicateTags",
                "s3:RestoreObject",
                "s3:ListBucket",
                "s3:GetAccelerateConfiguration",
                "s3:GetBucketPolicy",
                "s3:PutEncryptionConfiguration",
                "s3:GetEncryptionConfiguration",
                "s3:GetObjectVersionTorrent",
                "s3:AbortMultipartUpload",
                "s3:PutBucketTagging",
                "s3:GetBucketRequestPayment",
                "s3:GetAccessPointPolicyStatus",
                "s3:UpdateJobPriority",
                "s3:GetObjectTagging",
                "s3:GetMetricsConfiguration",
                "s3:GetBucketOwnershipControls",
                "s3:PutBucketVersioning",
                "s3:GetBucketPublicAccessBlock",
                "s3:ListBucketMultipartUploads",
                "s3:PutIntelligentTieringConfiguration",
                "s3:GetAccessPointPolicyStatusForObjectLambda",
                "s3:ListAccessPoints",
                "s3:PutMetricsConfiguration",
                "s3:PutStorageLensConfigurationTagging",
                "s3:PutBucketOwnershipControls",
                "s3:PutObjectVersionTagging",
                "s3:PutJobTagging",
                "s3:UpdateJobStatus",
                "s3:GetBucketVersioning",
                "s3:GetBucketAcl",
                "s3:GetAccessPointConfigurationForObjectLambda",
                "s3:PutInventoryConfiguration",
                "s3:ListStorageLensConfigurations",
                "s3:GetObjectTorrent",
                "s3:GetStorageLensConfiguration",
                "s3:GetAccountPublicAccessBlock",
                "s3:PutBucketWebsite",
                "s3:ListAllMyBuckets",
                "s3:PutBucketRequestPayment",
                "s3:PutObjectRetention",
                "s3:CreateAccessPointForObjectLambda",
                "s3:GetBucketCORS",
                "s3:GetBucketLocation",
                "s3:GetAccessPointPolicy",
                "s3:ReplicateDelete",
                "s3:GetObjectVersion",
                "kms:GetParametersForImport",
                "kms:DescribeCustomKeyStores",
                "kms:ListKeys",
                "kms:GetPublicKey",
                "kms:ListKeyPolicies",
                "kms:ListRetirableGrants",
                "kms:GetKeyRotationStatus",
                "kms:ListAliases",
                "kms:GetKeyPolicy",
                "kms:DescribeKey",
                "kms:ListResourceTags",
                "kms:ListGrants",
                "kms:Decrypt"
    ]
      resources = ["*"]
    }

}
