terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }

  backend "s3" {
    key             = "zeta_sso_permission_set.tfstate"
    dynamodb_table  = "terraform-state-lock"
    region          = "ap-south-1"
  }
}

provider "aws" {
  region  = var.aws_region
}
