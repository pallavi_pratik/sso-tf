variable "sso_name" {
  description = "The name of all the permission set that you want to create"
  default = {}
}

variable "aws_region" {
  description = "The region where you want to launch your resources"
  default = "ap-south-1"
}

variable "zeta_bu_lowercase" {
  description = "Name of the Business Unit in all lowercase"
  default = {}
}

variable "permission_set" {
  default = {}
}

variable "account_id" {
  default = {}
}

variable "inline_pm" {
  default = {}
}

variable "ecr_s3" {
  default = {}
}

variable "zeus_rcv2_stage_bucket_access" {
  default = {}
}

variable "s3_encrypt_access" {
  default = {}
}

variable "billing_read_aws_support_access" {
  default = {}
}

variable "s3_read_download_access" {
  default = {}
}

variable "dba_admin_permission_set" {
  default = {}
}

variable "datascience_permission_set" {
  default = {
    name                       = ""
    managed_permission_set     = []
    inline_permission_set      = []
  }
  type = object({
    name                       = string
    managed_permission_set     = list(string)
    inline_permission_set      = list(string)
  })
}
variable "dataengineering_permission_set" {
  default = {
    name                       = ""
    managed_permission_set     = []
    inline_permission_set      = []
  }
  type = object({
    name                       = string
    managed_permission_set     = list(string)
    inline_permission_set      = list(string)
  })
}
variable "athena_redshift_emr_access" {
  default = {}
}


variable "dba_permission_set" {
  default = {
    name                       = ""
    managed_permission_set     = []
    inline_permission_set      = []
  }
  type = object({
    name                       = string
    managed_permission_set     = list(string)
    inline_permission_set      = list(string)
  })
}

variable "data_devops_permission_set" {
  default = {
    name                       = ""
    managed_permission_set     = []
    inline_permission_set      = []
  }
  type = object({
    name                       = string
    managed_permission_set     = list(string)
    inline_permission_set      = list(string)
  })
}

variable "cloudfront_waf_permission_set" {
  default = {}
}

variable "datasync_iam_permission_set"{
  default = {}
}

variable "admin_pm_sso_permission_set" {
  default = {}
}

variable "admin_exp_permission_set" {
  default = {}
}

variable "admin_sso_permission_set" {
  default = {}
}

variable "admin_mgmt_permission_set" {
  default = {}
}

variable "cf_route53_permission_set" {
  default = {}
}


