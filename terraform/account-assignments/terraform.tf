terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }

  backend "s3" {
    key             = "zeta_sso_account_association.tfstate"
    dynamodb_table  = "terraform-state-lock"
    region          = "ap-south-1"
  }
}

provider "aws" {
  region  = var.aws_region
}
