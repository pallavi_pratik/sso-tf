data "aws_ssoadmin_instances" "sso_instance" {}

data "aws_identitystore_user" "this" {
  identity_store_id = tolist(data.aws_ssoadmin_instances.sso_instance.identity_store_ids)[0]
  count            = length(local.flatten-list)

filter {
  attribute_path  = "UserName"
  attribute_value = local.flatten-list[count.index].user_name
  }
}

locals {
  permission_set_details = {
    name = [for d in data.aws_ssoadmin_permission_set.this: d.name]
    arn = [for d in data.aws_ssoadmin_permission_set.this: d.arn]
 }
 permission_set_map = zipmap(local.permission_set_details.name, local.permission_set_details.arn)
}


 locals {
   user_id_details = {
   name = [for d in data.aws_identitystore_user.this: d.user_name]
   id = [for d in data.aws_identitystore_user.this: d.user_id]
  }
  user_details_map = zipmap(local.user_id_details.name, local.user_id_details.id)
}

data "aws_ssoadmin_permission_set" "this" {
  for_each  =    toset(flatten(local.flatten-list.*.permissions_set_name))
  instance_arn = tolist(data.aws_ssoadmin_instances.sso_instance.arns)[0]
  name         = each.value
}



resource "aws_ssoadmin_account_assignment" "account_id" {
  for_each = {for obj in local.flatten-list : "${obj.user_name}_${obj.account_id}_${obj.permissions_set_name}" => obj}
  instance_arn       = tolist(data.aws_ssoadmin_instances.sso_instance.arns)[0]
  permission_set_arn = local.permission_set_map[each.value["permissions_set_name"]]

  principal_id   = local.user_details_map[each.value["user_name"]]
  principal_type = "USER"

  target_id   = each.value["account_id"]
  target_type = "AWS_ACCOUNT"
}
