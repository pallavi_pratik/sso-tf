variable "sso_name" {
  description = "The name of all the permission set that you want to create"
  default = {}
}

variable "aws_region" {
  description = "The region where you want to launch your resources"
  default = "ap-south-1"
}

variable "zeta_bu_lowercase" {
  description = "Name of the Business Unit in all lowercase"
  default = {}
}

variable "permission_set" {
  default = {}
}

variable "account_id" {
  default = {}
}
