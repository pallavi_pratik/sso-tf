# AWS-SSO Terraform

The following module creates permission set and allow you to bind those permission_set with the AWS-SSO user.

# Usage

The user creation part is manual for that you must create your via console on aws. Rest the module is seperated in two part to allow permission_set creation and assignment of permission to a particular user in multiple accounts.

Currently Available Permission Sets Are :

- admin_permission_set (will provide admin access to the given account)
- read_only_permission_set (will provide read only access to given account)

### Creating Permission Set

To create a permission set move to

1) Clone the repo

2) Change Directory to the permission_set tfvars

```
cd   terraform/permission-sets/tfvars

```

3) Add you permission_set in the following manner such the the name of the permission_set will be a `key` and the
`policy_arn` will be a `list` to that key

```

permission_set  = {
  "<Name of the permission_set>" : ["ARN of the permission_set"],
  #Example 
  "s3_read_only_permission_set" : ["arn:aws:iam::aws:policy/AmazonS3ReadOnlyAccess"],
}
```

4) Check plan and Apply Terraform

### Attaching User To Permission Set & Account

1) Change Directory to the permission_set tfvars

```
cd   terraform/account-assignments/tfvars

```

2) Add a map with the following details

```
account_id = {
  "<account_id>" : {
    "<user_name>" : ["<permission_set_name>"],
}
}

#account_id: The account id for which you want the permission_set to bind
#user_name : The name of the user created via console
#permission_set_name : The name of the permission_set that you want to add to that user

```
